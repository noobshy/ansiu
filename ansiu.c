#include <stdio.h>
#include <locale.h>
#include <curses.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define PCOL 8192
#define PROW 4096
#define DEBUG
#define ESC 27
#define TUI_ROWS 24

char csi_char[16];

int buffer[PCOL][PROW];

int line_printable_chars=0;
int max_line_printable_chars=0;
char sauce_line[PCOL];
int sauce_width=0;
int sauce_heigth=0;
int sauce_found=0;
int file_sauce_begin=0;

int last_col_size=0;
int printed_chars=0;

int lines=0;
char *file_name;
char user_name[6]=".user";
char bookmark_file_name[1024];
int det_lines=0;
int ans_seq_flag=0;

int file_chars=0;
int rows=0;
int cols=0;
int max_cols=0;

int colorize=1;

int conv_table[2][256]={
{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255},
{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,199,252,233,226,228,224,229,231,234,235,232,239,238,236,196,197,201,230,198,244,246,242,251,249,255,214,220,162,163,165,8359,402,225,237,243,250,241,209,170,186,191,8976,172,189,188,161,171,187,9617,9618,9619,9474,9508,9569,9570,9558,9557,9571,9553,9559,9565,9564,9563,9488,9492,9524,9516,9500,9472,9532,9566,9567,9562,9556,9577,9574,9568,9552,9580,9575,9576,9572,9573,9561,9560,9554,9555,9579,9578,9496,9484,9608,9604,9612,9616,9600,945,223,915,960,931,963,181,964,934,920,937,948,8734,966,949,8745,8801,177,8805,8804,8992,8993,247,8776,176,8729,183,8730,8319,178,9632,160}
};

void scr_reset_video_attr() {
  printf ("\e[m");
}

int conv2utf8(int charin) {

  int r=0;

  if (r<256) {
    r=conv_table[1][charin];
  } else {
    r=' ';
  }

  return(r);

}

int detect_esc(int c) {
  if (c==27) {
    return(1);
  }
  return(0);
}

int detect_m(int c) {
  if (c=='m') {
    return(1);
  }
  return(0);
}

int detect_c(int c) {
  if (c=='C') {
    return(1);
  }
  return(0);
}

int detect_cr(int c) {
  if (c==13) {
    return(1);
  }
  return(0);
}

int is_digit(int c) {
  if (c>='0'&&c<='9') {
    return (1);
  }
  return (0);
}

int detect_eol(int c) {
  if (c==10) {
    return(1);
  }
  return(0);
}

void count_add_characters(int len) {
  line_printable_chars+=len;
}

void count_reset_printable_chars() {
  line_printable_chars=0;
}

void count_add_character() {
  line_printable_chars++;
}

int get_csi_shift() {

  int int_shift=0;
  char char_shift[5];

  if (is_digit(csi_char[1]) && is_digit(csi_char[2])) {
    char_shift[0]=csi_char[1];
    char_shift[1]=csi_char[2];
    char_shift[2]='\0';
    int_shift=atoi(char_shift);
  } else if (is_digit(csi_char[2]) && !is_digit(csi_char[1])) {
    char_shift[0]=csi_char[2];
    char_shift[1]='\0';
    int_shift=atoi(char_shift);
  } else if (is_digit(csi_char[1]) && !is_digit(csi_char[2])) {
    char_shift[0]=csi_char[1];
    char_shift[1]='\0';
    int_shift=atoi(char_shift);
  }

  return (int_shift);

}

void init_buffer() {

  int i;
  int j;

  for (j=0;j<PROW;j++) {
    for (i=0;i<PCOL;i++) {
      buffer[i][j]=0;
    }
  }
}

void get_sauce_line(int sauce_line_no) {

  int i=0;
  int found_sau_on_line=0;
  int diff_sau_late=0;

  for (i=0;i<PCOL-4;i++) {
    if ((buffer[i][sauce_line_no]=='S'&&
         buffer[i+1][sauce_line_no]=='A'&&
         buffer[i+2][sauce_line_no]=='U')
     && (found_sau_on_line==0)
    ) {
      found_sau_on_line=1;
    } else if (found_sau_on_line==0)  {
      diff_sau_late++;
    }
    if (found_sau_on_line) {
      sauce_line[i-diff_sau_late]=buffer[i][sauce_line_no];
    }
  }

}

int count_printable_chars(int c,int row,int col) {

  int csi_mov=0;

  if (ans_seq_flag==0) {
    if (detect_esc(c)) {
      ans_seq_flag=1;
    } else if (detect_cr(c)) {
      ;
    } else if (detect_eol(c)) {
      count_add_character();
    } else {
      count_add_character();
    }
  } else {
    if (detect_esc(c)) {
      ;
    } else if (detect_m(c)) {
      ans_seq_flag=0;
    } else if (detect_c(c)) {
      int i=col;
      int j=0;
      for (i=col-3;i<=col;i++) {
        csi_char[j]=buffer[i][row];
        j++;
      }
      csi_char[j]='\0';
      csi_mov=get_csi_shift();
      count_add_characters(csi_mov);
      ans_seq_flag=0;
    }
  }

  return (0);

}

int get_read_data_size() {

  return(file_chars);

}


int mseek_from_end(int shift) {

  int c=0; int i=0; int last_char_pos=0;

  int end_of_file = get_read_data_size();

  if (end_of_file-shift<=0) { return (-1); }

  for (i=0;i<PCOL-1;i++) {
    c=buffer[i][rows-1];
    if (c!=0 && buffer[i+1][rows]!=0) {
    } else {
      last_char_pos=i;
      break;
    }
  }

  return(last_char_pos);

}

void erase_sauce_from_memory() {

  char sauce_tag_exp[6]="SAUCE";
  int sauce_chars_detected=0;
  int i=mseek_from_end(128);
  int j=rows-4;
  int pos_sauce_delete=0;
  int eraser=0; int c;

  for (j=rows-4;i<rows;j++) {
    for (i=0;i<PCOL-1;i++) {
      c=buffer[i][j];
      if (c==sauce_tag_exp[sauce_chars_detected]) {
        sauce_chars_detected++;
        if (sauce_chars_detected==4) { eraser=1; pos_sauce_delete=i; }
        if (eraser) { buffer[i][rows-1]=0; }
      }
    }
  }
  if (eraser) { 
    for (i=0;i<pos_sauce_delete+4;i++) {
       buffer[i][rows-1]=0; 
    }
  }

}

int sauce_get_width(int sauce_line_no) {

  get_sauce_line(sauce_line_no);

  return (sauce_line[96]);

}

void decode_sauce(int line) {

  sauce_width=sauce_get_width(line);

}


int search_for_sauce(char *file_name) {

  FILE *file;
  file = fopen(file_name, "rb+");
  int hcomp=0;

  char sauce_tag[6];
  char sauce_tag_exp[6]="SAUCE";

  unsigned int width=0;
  unsigned int height=0;

  if (file) {
    fseek(file, -128L, SEEK_END);
    hcomp=fread(&sauce_tag,sizeof(sauce_tag),1,file);
    if (strncmp(sauce_tag,sauce_tag_exp,sizeof(sauce_tag))!=0) {
      return (0);
    }
    fseek(file, -32L, SEEK_END);
    file_sauce_begin=ftell(file);
    hcomp=fread(&width,sizeof(char),1,file);
    sauce_width=width;
    fseek(file, -30L, SEEK_END);
    hcomp=fread(&height,sizeof(char),1,file);
    sauce_heigth=height;
    fclose(file);
    if (hcomp) { ; }
  }

  return(1);

}

int is_ctrl_char(char in) {

  int ret=0;

  if (in == ESC) {
    ret=0;
  } 
  return (ret);

}

int is_ctrl_cont(char in) {

  int ret=0;
  
  if (in >= 0x40 && in <= 0x5F) {
    ret=1;
  } 

  return (ret);

}

int get_ansi_seq(char in, char in2) {

  int ret=0;
  
  if (is_ctrl_char(in) && is_ctrl_cont(in2)) {
    ret=1;
  }

  return(ret);

}

void print_ansi_file() {

  int i;
  int j;
  int c;
  int pc;

  count_reset_printable_chars();

  for (j=0;j<rows;j++) {
   for (i=0;i<max_cols;i++) {
      c=buffer[i][j];
      printf("%lc",c);
      count_printable_chars(c,j,i);
      if (line_printable_chars>=sauce_width
       && ans_seq_flag==0  && c!=10
      ) {
        count_reset_printable_chars();
        if (c!=10 || c!=13) { printf("\r\n"); }
        break;
      } else {
        printed_chars++;
      }
    }
    //printf("%d ",j);
  }
}

void print_page(int shift) {

  int i=0;
  int j=0;
  int c=0;

  count_reset_printable_chars();

  for (j=shift;j<TUI_ROWS+shift;j++) {
    for (i=0;i<max_cols;i++) {
      c=buffer[i][j];
      printf("%lc",c);
      count_printable_chars(c,j,i);
      if (line_printable_chars>=sauce_width
       && ans_seq_flag==0
      ) {
        count_reset_printable_chars();
        break;
      } else {
        printed_chars++;
      }
    }
  }
}

void convert_buffer() {

  int i,j,c;

  for (j=0;j<PROW;j++) {
    for (i=0;i<PCOL;i++) {
      c=conv2utf8(buffer[i][j]); 
      if (c==10) { break; }
    }
    if (j==10) { break; }
  } 
}


void count_lines_in_file(char *fName) {

  FILE *file;
  int c=0;

  file = fopen(fName, "r");
  if (file) {
    while (c != EOF) {
      c = getc(file);
      if (cols>PCOL || c==10) { 
        rows++; 
      }
    }
    fclose(file);
  } else {
    printf("File not found\n"); 
    exit(1);
  }
}

void load_ansi_file(char *fName) {

  FILE *file;
  file = fopen(fName, "r");
  int c;

  if (file) {
    do {
      c=getc(file);
      if (c==0) { c=' '; }
      file_chars++; 
      if (c!='\n'&&c!='\r' && cols < PCOL) { 
        buffer[cols][rows]=conv2utf8(c); 
      }
      count_printable_chars(c,rows,cols);
      if (line_printable_chars>=max_line_printable_chars) {
        max_line_printable_chars=line_printable_chars;
      }
      if ((line_printable_chars >= sauce_width 
       && ans_seq_flag == 0)
       || c=='\n') {
        count_reset_printable_chars();
        rows++;
        cols=0;
      } else {
        cols++;
      }
      if (cols>max_cols) { max_cols=cols; }
    } while (c!=EOF);
    last_col_size=cols;
    fclose(file);
  } else {
    printf("File not found\n"); 
    exit(1);
  }

}

void check_term() {

  if (COLS<sauce_width) {
    printf("Terminal size %d is less then %d required by ans file\n",
            COLS,sauce_width);
    exit(1);
  }

}

WINDOW *create_newwin(int height, int width, int starty, int startx) {
  WINDOW *local_win;

  local_win = newwin(height, width, starty, startx);
  box(local_win, 0 , 0);          
  wrefresh(local_win);           
  return local_win;

}

int auto_scroll() {

  int shift = 0;

  while (shift < rows-TUI_ROWS) {
    print_page(shift);
    timeout(60);
    if (getch()=='a') { break; }
    sleep(1);
    shift++;
  }
  return (shift);

}

void help_window(int shift) {

  WINDOW *help_win;

  help_win=create_newwin(TUI_ROWS,COLS,00,00);
  box(help_win,0,0);
  wrefresh(help_win);
  help_win=create_newwin(TUI_ROWS,COLS,00,00);
  wrefresh(help_win);
  mvprintw(1,2,"Displaying file: %s", file_name);
  mvprintw(2,2,"Your terminal dimensions are: %d %d", COLS, TUI_ROWS);
  mvprintw(3,2,"File has %d rows",rows);
  mvprintw(4,2,"Your are on %d position in file",shift);
  mvprintw(5,2,"Your terminal dimensions are: %d %d", COLS, TUI_ROWS);
  mvprintw(6,2,"Press <r> to return to ANSI file", TUI_ROWS, COLS);
  mvprintw(7,2,"Press <h> to display this Help screen", TUI_ROWS, COLS);
  mvprintw(8,2,"Press <n> in ANSI file to scroll page down", TUI_ROWS, COLS);
  mvprintw(9,2,"Press <u> in ANSI file to scroll page up", TUI_ROWS, COLS);
  mvprintw(10,2,"Press <j> in ANSI file to scroll line down", TUI_ROWS, COLS);
  mvprintw(11,2,"Press <k> in ANSI file to scroll line up", TUI_ROWS, COLS);
  mvprintw(12,2,"Press <a> in ANSI file to auto-scroll", TUI_ROWS, COLS);
  mvprintw(13,2,"Press <a> again in ANSI file auto-scroll to stop", TUI_ROWS, COLS);
  mvprintw(14,2,"Press <b> to bookmark Your current position", TUI_ROWS, COLS);
  mvprintw(15,2,"Press <g> go to Your last position", TUI_ROWS, COLS);

}

int read_pos_from_file() {

  int shift=0;

  FILE *f = fopen(bookmark_file_name, "r");

  if (f) {
    if (f == NULL) { ; }
    if (fscanf(f, "%d", &shift) == 1) {
    }
    fclose(f);
  } else {
    printf("Bookmark file doesn't exists\n");
  }
  
  return(shift);

}

void save_pos_in_file(int shift) {

  FILE *f = fopen(bookmark_file_name, "w");

  if (f) { 
    fprintf(f, "%d", shift);
    fclose(f);
  } else {
    printf("Error opening bookmark file!\n");
  }

}

void keyboard_handler() {

  char key='-';
  int shift=0;
  int render=1;


  while (key!='q') {
    if (key=='j') {
      if (shift<rows-TUI_ROWS-1) { shift++; render=1; }
    } else if (key=='k') {
      if (shift>0) { shift--; render=1; }
    } else if (key=='n' && shift != rows-TUI_ROWS-1) {
      if (shift+TUI_ROWS<rows-TUI_ROWS) { shift+=TUI_ROWS; render=1; }
      else { shift=rows-TUI_ROWS-1; render=1; }
    } else if (key=='u' && shift != 0) {
      if (shift-TUI_ROWS>0) { shift=shift-TUI_ROWS; render=1; }
      else { shift=0; render=1;}
    } else if (key=='-') {
      render=1;
    } else if (key=='h') {
      help_window(shift);
    } else if (key=='r') {
      render=1;
    } else if (key=='a') {
      shift = auto_scroll();
    } else if (key=='b') {
      save_pos_in_file(shift);
    } else if (key=='g') {
      shift = read_pos_from_file();
      render=1;
    } else {
      render=0;
    }
    if (render==1) {
      clear();
      refresh();
      if (shift+TUI_ROWS<rows) {
        print_page(shift);
      } else {
        printf("ERROR");
      }
      render=0;
    }
    key=getch();
  }
}

void init_bookmark_file_name() {
  sprintf(bookmark_file_name,"%s%s.bmk",file_name,user_name);
}

void init_console() {
  scr_reset_video_attr();
  setlocale(LC_ALL, "");
  initscr();
  nonl(); 
  noecho();
}

void deinit_console(int mode) {
  endwin();
  nl();
  echo();
  if (mode==1) { printf ("\e[2J\e[0;0H\e[1;37;40m\r\n"); }
  if (mode==2) { scr_reset_video_attr(); }
}

void process_sauce(char *fname) {
  sauce_found=search_for_sauce(fname);
  if (sauce_found==0) {
    sauce_width=80;
  }
}

int main(int argc, char **argv) {

  file_name = argv[1];

  init_buffer();
  init_console();
  process_sauce(file_name);
  load_ansi_file(file_name);
  erase_sauce_from_memory();
  init_bookmark_file_name();
  if (argc==3) {
    keyboard_handler();
    deinit_console(1);
  } else if (argc==2) {
    print_ansi_file();
    deinit_console(2);
  } else {
    printf("Usage: ansiu <filename.ans> <-i>\n");
  }

  return (0);

}
